import tensorflow as tf
from tensorflow import keras

import cv2
import numpy as np

# loading pretrained model
model = tf.keras.models.load_model("improved_model_v2.h5")

# define kategori output
kategori = [
    "ba", "ca", "da",  "dha", "ga", "ha",  "ja", "ka", "la",  "ma",  
    "na",  "nga", "nya", "pa",  "ra",  "sa", "ta", "tha",  "wa",  "ya",
]

# define window & parameter
window_name = "Image"
font = cv2.FONT_HERSHEY_SIMPLEX
thickness = 2
fontScale = 1
org = (50, 50)
color1 = (0, 255, 0)
color2 = (0, 0, 255)


# define fungsi preprocessing tahap 1
def stage1_preprocessing(raw_image):

    stage1_preprocessed = cv2.resize(raw_image, (100, 100))
    stage1_preprocessed = cv2.cvtColor(stage1_preprocessed, cv2.COLOR_BGR2GRAY)
    stage1_preprocessed = cv2.adaptiveThreshold( stage1_preprocessed, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 21,4,)

    return stage1_preprocessed


# define fungsi preprocessing tahap 2 dan penyesuaian input ( image => input model )
def prepare(preprocessed_stage1):

    # preprocessing morfologi
    kernel1 = np.ones((3, 3), np.uint8)
    morph_preprocessed = cv2.erode(preprocessed_stage1, kernel1, iterations=1)
    morph_preprocessed = cv2.dilate(morph_preprocessed, kernel1, iterations=1)

    # persiapan input model 2D (w,w) => 4D (1,W,W,1)
    input_model = np.reshape(morph_preprocessed, (100, 100, 1))
    input_model = input_model / 255
    input_model = np.expand_dims(input_model, axis=0)
    input_model = np.vstack([input_model])

    return input_model


# video capture - http stream
vid_stream = cv2.VideoCapture("http://192.168.1.6:4747/video?640x480")

# define loop1
loop1 = 0

# define huruf
huruf = "belum mendeteksi"

while True:

    ret, frame = vid_stream.read()
    if ret != True:
        break

    # preprocessing tahap 1 - binary - 100*100
    image1 = stage1_preprocessing(frame)

    # L - jika kertas berisi huruf ( distribusi 5% ideal => 240)
    if np.mean(image1) <= 240:

        loop1 += 1
        frame = cv2.putText(frame, huruf, org, font, fontScale, color1, thickness, cv2.LINE_AA )

    # L - jika huruf tidak terdeteksi
    else:

        frame = cv2.putText( frame, "tidak ada", org, font, fontScale, color2, thickness, cv2.LINE_AA)
        huruf = "belum mendeteksi"

    # L - jika huruf terdeteksi di 100 frame
    if loop1 >= 100:

        # klasifikasi huruf terbaca
        input_model = prepare(image1)
        result = model.predict(input_model, batch_size=8)
        huruf = kategori[np.argmax(result)]

        print(huruf)
        loop1 = 0

    cv2.imshow("frame", frame)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break


# clossing
vid_stream.release()
cv2.destroyAllWindows()
